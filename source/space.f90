subroutine StringUtility_NoSpace(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  integer :: i,ii,iend
  integer, parameter :: ascii_code_space = 32 
  character(1), parameter :: space = " "

  iend = len_trim(str)
  ii = 0
  do i=1,iend
    if ( iachar(str(i:i)) == ascii_code_space ) then
      cycle
    end if

    ii = ii + 1
    str(ii:ii) = str(i:i)
  end do

  do i=ii+1,iend
    str(i:i) = space
  end do

  return
end subroutine

!===============================================================================

subroutine StringUtility_NoTab(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  integer :: i,ii,iend
  integer, parameter :: ascii_code_tab = 9 
  character(1), parameter :: space = " "

  iend = len_trim(str)
  ii = 0
  do i=1,iend
    if ( iachar(str(i:i)) == ascii_code_tab ) then
      cycle
    end if
    
    ii = ii + 1
    str(ii:ii) = str(i:i)
  end do

  do i=ii+1,iend
    str(i:i) = space
  end do
  
  return
end subroutine

!===============================================================================

subroutine StringUtility_NoBlankChar(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  
  call StringUtility_NoTab(str)
  call StringUtility_NoSpace(str)
  return
end subroutine

!===============================================================================
!===============================================================================
!===============================================================================

subroutine StringUtility_TabToSpace(str,nSpace)
  implicit none
  character(*), intent(inout) :: str
  integer,      intent(in)    :: nspace
  !----------------------------
  character(100000) :: sdum1
  integer :: nsp,isp
  integer :: i,ii,iend
  character(1), parameter :: space = " "
  integer, parameter :: ascii_code_tab = 9 
  
  !--- Check option ---
  if ( nspace >= 0 ) then
    nsp = nspace
  else
    nsp = 1
  end if
  
  !--- Replace tabs to spaces ---
  sdum1 = trim(str)
  iend = len_trim(sdum1)
  ii = 0
  do i=1,iend
    if ( iachar(sdum1(i:i)) == ascii_code_tab ) then
      do isp=1, nsp
        ii = ii + 1
        str(ii:ii) = space
      end do
      cycle
    end if
    
    ii = ii + 1
    str(ii:ii) = sdum1(i:i)
  end do
  
  return
end subroutine

!==============================================================================
