module StringUtility
  implicit none
  
  interface ToUpper
    subroutine StringUtility_ToUpper(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface ToLower
    subroutine StringUtility_ToLower(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface Capitalize
    subroutine StringUtility_Capitalize(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface ToUpperNoBlankChar 
    subroutine StringUtility_ToUpperNoBlankChar(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface NoSpace 
    subroutine StringUtility_NoSpace(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface NoTab 
    subroutine StringUtility_NoTab(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface NoBlankChar 
    subroutine StringUtility_NoBlankChar(str)
      character(*), intent(inout) :: str
    end subroutine  
  end interface
  !--------------------------
  interface split 
    subroutine StringUtility_split_by_string(str,divstr,dim_divstr,delimeter,ndiv,istat)
      integer,        intent(in)  :: dim_divstr
      character(*),   intent(in)  :: str
      character(*),   intent(in)  :: delimeter
      integer,        intent(out) :: ndiv
      integer,        intent(out) :: istat
      character(*),   intent(out) :: divstr(1:dim_divstr)
    end subroutine  
    !--------------------------
    subroutine StringUtility_split_by_strings(str,divstr,dim_divstr,delimeters,ndiv,istat)
      integer,        intent(in)  :: dim_divstr
      character(*),   intent(in)  :: str
      character(*),   intent(in)  :: delimeters(:)
      integer,        intent(out) :: ndiv
      integer,        intent(out) :: istat
      character(*),   intent(out) :: divstr(1:dim_divstr)
    end subroutine  
  end interface
  !--------------------------
  interface join
    subroutine StringUtility_join(             &
                                 & strings,    &
                                 & nstrings,   &
                                 & joined,     &
                                 & separator,  &
                                 & istat       )
      implicit none
      integer(4),     intent(in)  :: nstrings
      character*(*),  intent(in)  :: strings(1:nstrings)
      character*(*),  intent(in)  :: separator
      character*(*),  intent(out) :: joined
      integer(4),     intent(out) :: istat
    end subroutine
  end interface

end module

!===============================================================================

