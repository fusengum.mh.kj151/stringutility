subroutine StringUtility_ToUpper(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  integer :: i,iend
  
  iend = len_trim(str)
  do i=1, iend
    if(str(i:i) >= 'a' .and. str(i:i) <= 'z') then
      str(i:i) = char(ichar(str(i:i))-32)
    end if
  end do
  
  return
end subroutine

!===============================================================================

subroutine StringUtility_ToLower(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  integer :: i,iend
  
  iend = len_trim(str)
  do i=1, iend
    if(str(i:i) >= 'A' .and. str(i:i) <= 'Z') then
      str(i:i) = char(ichar(str(i:i))+32)
    end if
  end do
  
  return
end subroutine

!===============================================================================

subroutine StringUtility_Capitalize(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  
  call StringUtility_toLower(str)
  if(str(1:1) >= 'a' .and. str(1:1) <= 'z') then
    str(1:1) = char(ichar(str(1:1))-32)
  end if
  
  return
end subroutine

!===============================================================================

subroutine StringUtility_ToUpperNoBlankChar(str)
  implicit none
  character(*), intent(inout) :: str
  !----------------------------
  
  call StringUtility_ToUpper(str)
  call StringUtility_NoSpace(str)
  call StringUtility_NoTab(str)
  return
end subroutine

!===============================================================================


