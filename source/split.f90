subroutine StringUtility_split_two(             &
                                  & original,   &
                                  & divided,    &
                                  & remained,   &
                                  & delimiters, &
                                  & istat       )
  implicit none
  character(*),   intent(in)  :: original
  character(*),   intent(in)  :: delimiters(:)
  character(*),   intent(out) :: divided
  character(*),   intent(out) :: remained
  integer(4),     intent(out) :: istat
  !----------------------------
  character(1), parameter :: space = " "
  integer :: ipos
  integer :: l_del
  integer :: imin1, imax1, imin2, imax2
  integer :: i
  integer :: ndelim
  
  istat = 0

  imin1 = 1
  imax1 = -1 
  imin2 = -1
  imax2 = len_trim(original)
 
  if ( imax2 == 0 ) then
    divided = "" 
    remained = ""
    istat = -1
    return
  end if
  
  ndelim = size(delimiters) 
  do i=1,ndelim
    if ( len_trim(delimiters(i)) == 0 ) then
      l_del = 1
      ipos = index(trim(original),space)
    else
      l_del = len_trim(delimiters(i))
      ipos = index(trim(original),trim(delimiters(i)))
    endif
    
    if ( ipos == 0 ) then
      cycle
    else if ( ipos > 0 ) then 
      if ( imin2 > ipos + l_del) then
        !imin1 = 1
        imax1 = ipos - 1
        imin2 = ipos + l_del
        !imax2 = len_trim(original)
      end if 
    else 
      cycle
    end if
  end do
  
  if ( (imax1 == -1) .and. (imin2 == -1) ) then
    divided = trim(original)
    remained = ""
    istat = -1
  else if ( imax1 == 0 ) then
    divided = ""
    remained = trim(original)
  else if ( imin2 > imax2 ) then
    divided = trim(original)
    remained = ""
    !istat = -1
  else 
    divided = original(1:imax1)
    remained = original(imin2:imax2)
  end if
  
  return
end subroutine

!==============================================================================
!==============================================================================
!==============================================================================

subroutine StringUtility_split_by_string(              &
                                        & original,    &
                                        & divided,     &
                                        & max_divided, &
                                        & delimiter,   &
                                        & n_divided,   &
                                        & istat        &
                                        &              )
  implicit none
  integer,        intent(in)  :: max_divided
  character(*),   intent(in)  :: original
  character(*),   intent(in)  :: delimiter
  integer,        intent(out) :: n_divided
  character(*),   intent(out) :: divided(1:max_divided)
  integer,        intent(out) :: istat
  !----------------------------
  interface 
    subroutine StringUtility_split_by_strings(              &
                                             & original,    &
                                             & divided,     &
                                             & max_divided, &
                                             & delimiters,  &
                                             & n_divided,   &
                                             & istat        &
                                             &              )
      integer,        intent(in)  :: max_divided
      character(*),   intent(in)  :: original
      character(*),   intent(in)  :: delimiters(:)
      integer,        intent(out) :: n_divided
      character(*),   intent(out) :: divided(1:max_divided)
      integer,        intent(out) :: istat
    end subroutine
  end interface
  !----------------------------
  character(:), allocatable :: delimiters(:)
  integer :: ssize
  
  istat = 0
  ssize = len_trim(delimiter) 
  allocate( character(ssize) :: delimiters(1) )
  delimiters(1) = trim(delimiter)

  
  call StringUtility_split_by_strings(              &
                                     & original,    &
                                     & divided,     &
                                     & max_divided, &
                                     & delimiters,  &
                                     & n_divided,   &
                                     & istat        &
                                     &              )

  deallocate( delimiters )
  return 
end subroutine

!==============================================================================

subroutine StringUtility_split_by_strings(              &
                                         & original,    &
                                         & divided,     &
                                         & max_divided, &
                                         & delimiters,  &
                                         & n_divided,   &
                                         & istat        &
                                         &              )
  implicit none
  integer,        intent(in)  :: max_divided
  character(*),   intent(in)  :: original
  character(*),   intent(in)  :: delimiters(:)
  integer,        intent(out) :: n_divided
  character(*),   intent(out) :: divided(1:max_divided)
  integer,        intent(out) :: istat
  !----------------------------
  interface
    subroutine StringUtility_split_two(             &
                                      & original,   &
                                      & divided,    &
                                      & remained,   &
                                      & delimiters, &
                                      & istat       )
      implicit none
      character(*),   intent(in)  :: original
      character(*),   intent(in)  :: delimiters(:)
      character(*),   intent(out) :: divided
      character(*),   intent(out) :: remained
      integer(4),     intent(out) :: istat
    end subroutine
  end interface
  !----------------------------
  integer :: i
  integer :: itermax
  integer :: nsdum
  character(:), allocatable :: sdum1,sdum2
  
  istat = 0

  !--- Check delimiters ---
  nsdum = len_trim(original)
  itermax = nsdum 
  allocate( character(nsdum) :: sdum1 )
  allocate( character(nsdum) :: sdum2 )

  sdum1 = trim(original)
  n_divided = 0
  do i=1,itermax
    call StringUtility_split_two( sdum1,sdum2,sdum1,delimiters,istat )
    if ( istat == -1 ) then
      exit
    else 
      n_divided = n_divided + 1
      divided(n_divided) = trim(sdum2)
    end if
  end do

  deallocate( sdum1 )
  deallocate( sdum2 )

  return 
end subroutine

!==============================================================================








