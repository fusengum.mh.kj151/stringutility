subroutine StringUtility_join(             &
                             & strings,    &
                             & nstrings,   &
                             & joined,     &
                             & separator,  &
                             & istat       )
  implicit none
  integer(4),     intent(in)  :: nstrings
  character*(*),  intent(in)  :: strings(1:nstrings)
  character*(*),  intent(in)  :: separator
  character*(*),  intent(out) :: joined
  integer(4),     intent(out) :: istat
  !----------------------------
  character(1), parameter :: space = " "
  !----------------------------
  integer(4) :: i, lse 
  
  if ( nstrings == 0 ) then
    joined = ""
    return
  end if

  lse = len_trim(separator)
  joined = trim(strings(1))
  do i=2,nstrings
    if ( lse == 0 ) then
      joined = trim(joined) // space // trim(strings(i))
    else 
      joined = trim(joined) // trim(separator) // trim(strings(i))
    end if  
  end do

  return
end subroutine


