#!/usr/bin/env python3

###
### Modules
###
import sys
import json 
import yaml
import pathlib
import argparse
#import logger
import jinja2 
import itertools

###
### Argument configuration
###
parser = argparse.ArgumentParser(
	prog='Template translater for Jinja2 format',
	#usage='',
	#description='description',
	#epilog='end', 
	add_help=True, 
)

parser.add_argument(
	"inputfiles", 
	help="Original template files",
	action="append",
	#required=True,
)
parser.add_argument(
	"-c","--configfiles", 
	help="Configuration files for template variables.",
	action="append",
	default=[],
)
parser.add_argument(
	"-o","--outputpath", 
	help="Output path for created files.",
	default=None,
)
parser.add_argument(
	"-s","--suffix",
	help="Name added at last of a file name with a dot.",
	default=".f90",
)


###
### User functions
###
def ErrorMessage(msg):
	print(f"[ERROR] {msg}")
	return 0

###
### Main
###

### Get arguments ###
_args = parser.parse_args()

### Define global variables ###
_params = {}
_outputpath = _args.outputpath
_suffix = _args.suffix
if not _suffix.startswith(".") :
	_suffix = f".{_suffix}"


### Load configs ###
print(_args.configfiles)
for f_c in _args.configfiles :
	target = pathlib.Path(f_c.strip()).resolve()
	if not target.exists() :
		ErrorMessage(f"Not found for a config file ({target})")
		sys.exit(1)
	
	with open(target,"r") as f :
		if target.suffix == ".json" : 
			params_t = json.load(f)
		elif target.suffix in [".yaml",".yml"] :
			params_t = yaml.safe_load(f)
		else :
			params_t = yaml.safe_load(f)
	
	if params_t is not None :
		_params = {
			**_params,
			**params_t,
		}
	
	continue

### Translate ###
for f_i in _args.inputfiles :
	target = pathlib.Path(f_i).resolve()
	if not target.exists() :
		ErrorMessage(f"Not found for a template file ({target})")
		sys.exit(1)
	
	if _outputpath is None :
		outputfile = pathlib.Path(f"{target}{_suffix}")
	else :
		outputfile = pathlib.Path(f"{_outputpath}/{target.name}{_suffix}")
	
	fsl = jinja2.FileSystemLoader(searchpath=f"{target.parent}")
	env = jinja2.Environment(loader=fsl)
	env.filters["combinations"] = itertools.product
	tmpl = env.get_template(f"{target.name}")
	tmpl_o = tmpl.render(_params)
	
	with open(outputfile,"w") as f_o :
		f_o.write(tmpl_o)
	
	continue




