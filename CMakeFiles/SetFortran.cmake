###
### Compiler setting for Fortran
###

### Get environmental valiable ###
set(ENV_Fortran_COMPILER "$ENV{FC}")

### Set user valiable ###
set(Fortran_COMPILER_INTEL_CMD  "ifort"     )
set(Fortran_COMPILER_PGI_CMD    "pgfortran" )
set(Fortran_COMPILER_NVIDIA_CMD "nvfortran" )
set(Fortran_COMPILER_GNU_CMD    "gfortran"  )


### Set compiler with priority ###
if(CMAKE_Fortran_COMPILER)
	set(USER_Fortran_COMPILER ${CMAKE_Fortran_COMPILER})
elseif(Fortran_COMPILER)
	set(USER_Fortran_COMPILER ${ENV_Fortran_COMPILER})
else()
	execute_process(
		COMMAND which ${Fortran_COMPILER_INTEL_CMD}
		OUTPUT_VARIABLE Fortran_COMPILER_INTEL_PATH
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	execute_process(
		COMMAND which ${Fortran_COMPILER_PGI_CMD}
		OUTPUT_VARIABLE Fortran_COMPILER_PGI_PATH
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	execute_process(
		COMMAND which ${Fortran_COMPILER_NVIDIA_CMD}
		OUTPUT_VARIABLE Fortran_COMPILER_NVIDIA_PATH
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	execute_process(
		COMMAND which ${Fortran_COMPILER_GNU_CMD}
		OUTPUT_VARIABLE Fortran_COMPILER_GNU_PATH
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)

	if(Fortran_COMPILER_INTEL_PATH)
		set(USER_Fortran_COMPILER "${Fortran_COMPILER_INTEL_PATH}")
	elseif(Fortran_COMPILER_NVIDIA_PATH)
		set(USER_Fortran_COMPILER "${Fortran_COMPILER_NVIDIA_PATH}")
	elseif(Fortran_COMPILER_PGI_PATH)
		set(USER_Fortran_COMPILER "${Fortran_COMPILER_PGI_PATH}")
	elseif(Fortran_COMPILER_GNU_PATH)
		set(USER_Fortran_COMPILER "${Fortran_COMPILER_GNU_PATH}")
	else()
		set(USER_Fortran_COMPILER "")
	endif()
endif()


### Validate Fortran compiler ###
set(CMAKE_Fortran_COMPILER "${USER_Fortran_COMPILER}")
enable_language(Fortran)
message(STATUS "Fortran compiler    : ${CMAKE_Fortran_COMPILER}")
message(STATUS "Fortran compiler id : ${CMAKE_Fortran_COMPILER_ID}")


### Set compiler flags ###
if("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "Intel")
	
	add_definitions(-fpp)
	add_definitions(-DFORTRAN_COMPILER_INTEL)
	
	set(CMAKE_Fortran_MODDIR_FLAG "-module ")
	
	set(CMAKE_Fortran_FLAGS " -heap-arrays ") # -r8
	set(CMAKE_Fortran_FLAGS_DEBUG "-g -pg -traceback -warn all -check all -heap-arrays ")
	set(CMAKE_Fortran_FLAGS_RELEASE "-O3 ")
	set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-O3  -g -pg")
	
	set(CMAKE_EXE_LINKER_FLAGS "")
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG "")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "")
	set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "-g -pg")
	
	set(CMAKE_STATIC_LINKER_FLAGS "")
	set(CMAKE_STATIC_LINKER_FLAGS_DEBUG "")
	#set(CMAKE_STATIC_LINKER_FLAGS_RELEASE "-ipo")
	set(CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO "")
	
	set(CMAKE_SHARED_LINKER_FLAGS "")
	set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "")
	
	
elseif("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "PGI" OR "${CMAKE_Fortran_COMPILER_ID}" STREQUAL "NVIDIA")
	set(CMAKE_Fortran_MODDIR_FLAG "-module ")
	
	set(CMAKE_Fortran_FLAGS "") # -r8
	set(CMAKE_Fortran_FLAGS_DEBUG "-g -Minform=inform -Mdclchk -Mstandard -Mbounds -traceback")
	set(CMAKE_Fortran_FLAGS_RELEASE "-Minfo -fastsse -Mipa=fast,inline -notraceback")
	#set(CMAKE_Fortran_FLAGS_RELEASE "-Minfo=all -fastsse -notraceback")
	set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-fastsse -g -pg -notraceback -Minfo")
	
	set(CMAKE_EXE_LINKER_FLAGS "-pgf90libs")
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG "-g -pg")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-fastsse -notraceback -ipo")
	set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "-fastsse -notraceback -ipo -g -pg")
	
	set(CMAKE_STATIC_LINKER_FLAGS "")
	set(CMAKE_STATIC_LINKER_FLAGS_DEBUG "")
	set(CMAKE_STATIC_LINKER_FLAGS_RELEASE "")
	set(CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO "")
	
	set(CMAKE_SHARED_LINKER_FLAGS "")
	set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "")
	
	add_definitions(-Mpreprocess)
	add_definitions(-DFORTRAN_COMPILER_PGI)
	
elseif("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "GNU")
	set(CMAKE_Fortran_MODDIR_FLAG "-J")
	
	set(CMAKE_Fortran_FLAGS "-ffree-form -fno-range-check -ffree-line-length-none") # -fdefault-real-8
	set(CMAKE_Fortran_FLAGS_DEBUG "-g -Wall -Wextra -Warray-temporaries -fimplicit-none -fbacktrace -finit-real=nan -pg")
	set(CMAKE_Fortran_FLAGS_RELEASE "-O3 ")
	set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-O3 -g -pg")
	
	set(CMAKE_EXE_LINKER_FLAGS "")
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG "")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "")
	
	set(CMAKE_STATIC_LINKER_FLAGS "")
	set(CMAKE_STATIC_LINKER_FLAGS_DEBUG "")
	set(CMAKE_STATIC_LINKER_FLAGS_RELEASE "")
	
	set(CMAKE_SHARED_LINKER_FLAGS "")
	set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "")
	set(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "")
	
	if(CMAKE_Fortran_COMPILER_VERSION VERSION_GREATER "5.0.0")
		set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -fallow-invalid-boz")
	endif()
	
#	add_definitions(-ffree-form -ffree-line-length-none)
	add_definitions(-cpp)
	add_definitions(-DFORTRAN_COMPILER_GNU)
	
else()
	message(FATAL_ERROR "Any available Fortran compiler is not found!")
endif()
















