### 
### Common option for all compiler
###

### Set default build type ###
if("${CMAKE_BUILD_TYPE}" STREQUAL "")
	set(CMAKE_BUILD_TYPE Release)
endif()
message(STATUS "Build type : ${CMAKE_BUILD_TYPE}")

### OpenMP options ###
OPTION(USE_OpenMP "Enable OpenMP directive." ON)

### Test program options ###
OPTION(DO_TEST "Enable testing sets for this libraries." ON)


