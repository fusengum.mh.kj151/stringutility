###########################################################################
########################  Set external libraries ##########################
###########################################################################
include(ExternalProject)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_INSTALL_PREFIX}/cmake)



### ###
if(USE_OpenMP)
	#find_package(OpenMP REQUIRED)
	find_package(OpenMP)
	if(OpenMP_FOUND)
		if(CMAKE_C_COMPIER_ID)
			set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
		endif()
	
		if(CMAKE_CXX_COMPILER_ID)
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
		endif()
	
		if(CMAKE_Fortran_COMPILER_ID)
			set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_Fortran_FLAGS}")
		endif()
	endif()
endif()

###
###
###
if( DO_TEST )
	enable_testing()
	set(TARGET_TESTING_PACKAGE "TestSuite")
	find_package(${TARGET_TESTING_PACKAGE} QUIET)
	if(TestSuite_FOUND)
		#add_custom_target(
		#	${TARGET_TESTING_PACKAGE}
		#)
	else()
		ExternalProject_Add(
			${TARGET_TESTING_PACKAGE}
			GIT_REPOSITORY https://gitlab.com/fusengum.mh.kj151/testsuite.git
			GIT_TAG main
			PREFIX ${CMAKE_BINARY_DIR}
			#SOURCE_DIR "${CMAKE_BINARY_DIR}/${TARGET_TESTING_PACKAGE}/"
			#BINARY_DIR ${CMAKE_BINARY_DIR}/${TARGET_TESTING_PACKAGE}/_build
			CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR} -DCMAKE_BUILD_TYPE=Release -DDO_TEST=OFF
			BUILD_COMMAND make
			INSTALL_COMMAND make install
		)
	endif()
endif()




