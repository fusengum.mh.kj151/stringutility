###

cmake_minimum_required(VERSION 3.0)

###
### Requisite ###
find_package(Python3 COMPONENTS Interpreter)
if (Python3_EXECUTABLE)
else()
	message(FATAL_ERROR "Not defined of Python3_EXECUTABLE.")
endif()


#======================================
#======================================
#======================================

macro(LOAD_ENV_TEMPLATE_ENGINE)
	foreach(MODULE_PATH ${CMAKE_MODULE_PATH})
		set(TEMPLATE_ENGINE0 "${MODULE_PATH}/create.py")
		if(EXISTS ${TEMPLATE_ENGINE0})
			set(TEMPLATE_ENGINE ${TEMPLATE_ENGINE0})
		endif()
	endforeach()
	set(TEMPLATE_ENGINE_CONFIG_DIR "${CMAKE_SOURCE_DIR}/config")
	set(SRC_GENERATED_SUFFIX ".__gen__.f90")
	set(SRCS_GENERATED_ROOT_DIR "${CMAKE_BINARY_DIR}/sources_generated")
	if (NOT EXISTS ${SRCS_GENERATED_ROOT_DIR})
		file(MAKE_DIRECTORY ${SRCS_GENERATED_ROOT_DIR})
	endif()
	
	
	### List up parental config files ###
	file(
		GLOB_RECURSE confs 
		${TEMPLATE_ENGINE_CONFIG_DIR}/*.yaml  
		${TEMPLATE_ENGINE_CONFIG_DIR}/*.yml  
		${TEMPLATE_ENGINE_CONFIG_DIR}/*.json  
		${TEMPLATE_ENGINE_CONFIG_DIR}/*.macro 
	)
	
	set(configs "")
	foreach(item ${confs})
		list(APPEND configs "${item}")
	endforeach()
	
endmacro()

#======================================

function(template_to_source)
	LOAD_ENV_TEMPLATE_ENGINE()
	
	### Categorize files ###
	set(templates "")
	set(j2macros "")
	set(newsources "")
	foreach(arg ${ARGN})
		if ( "${arg}" MATCHES ".*\\.(yaml|yml|json)" )
			list(APPEND configs "${arg}")
		elseif ( "${arg}" MATCHES ".*\\.(macro)" )
			list(APPEND j2macros "${arg}")
		else()
			list(APPEND templates "${arg}")
		endif()
	endforeach()
	message(STATUS "Configs : ${configs}")
	message(STATUS "Template: ${templates}")
	
	### Create argument ###
	set(config_args "")
	foreach(arg ${configs})
		list(APPEND config_args "-c ${arg}")
	endforeach()
	#message(STATUS "Config_args : ${config_args}")
	
	### Create generation targets ###
	foreach(target ${templates})
		get_filename_component(FILE_DIR "${target}" DIRECTORY)
		get_filename_component(FILE_EXT "${target}" EXT)
		get_filename_component(FILE_NAME "${target}" NAME)
		get_filename_component(FILE_NAMEBODY "${target}" NAME_WE)
		
		set(OUTPUT_DIR "${SRCS_GENERATED_ROOT_DIR}/")
		if(NOT EXISTS ${OUTPUT_DIR})
			file(MAKE_DIRECTORY ${OUTPUT_DIR})
		endif()
		#message(STATUS "Argument : ${arg}")
		#message(STATUS "Input file : ${target}")
		#message(STATUS "Output directory : ${OUTPUT_DIR}")
		
		set(src "${OUTPUT_DIR}/${FILE_NAME}${SRC_GENERATED_SUFFIX}")
		list(APPEND newsources "${src}")
		
		if (TARGET "${src}")
		else()
			add_custom_command(
				OUTPUT "${src}"
				COMMAND ${Python3_EXECUTABLE} ${TEMPLATE_ENGINE} ${target} ${config_args} -s ${SRC_GENERATED_SUFFIX} -o ${OUTPUT_DIR}
				DEPENDS ${target} ${configs} ${j2macros}
			)
		endif()
	endforeach()
	
	set(TEMPLATE_TO_SOURCE_OUTPUT "${newsources}" PARENT_SCOPE)
	
endfunction()
