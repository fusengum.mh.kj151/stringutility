program test_join
  implicit none
  
  call test_join1()
  call test_join2()
  call test_join3()
 
end program

!==============================================================================
!==============================================================================
!==============================================================================

subroutine test_join1()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoSpace
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize), allocatable :: strings_in(:)
  character(ssize) :: separator
  character(ssize) :: results
  character(ssize) :: expected
  integer(4) :: nst
  integer(4) :: istat
  
  ! === Preparation ===
  separator = " "
  strings_in  = [character(ssize) :: "1a","2b","3c"]
  nst = size(strings_in)
  expected = "1a 2b 3c"
  
  call StringUtility_join( strings_in, nst, results, separator, istat)
  
  call isEqual(results,expected)

  deallocate( strings_in )
  return
end subroutine

!==============================================================================

subroutine test_join2()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoSpace
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize), allocatable :: strings_in(:)
  character(ssize) :: separator
  character(ssize) :: results
  character(ssize) :: expected
  integer(4) :: nst
  integer(4) :: istat
  
  ! === Preparation ===
  separator = ","
  strings_in  = [character(ssize) :: "1","2","3"]
  nst = size(strings_in)
  expected = "1,2,3"
  
  call StringUtility_join( strings_in, nst, results, separator, istat)
  
  call isEqual(results,expected)
  
  deallocate( strings_in )
  return
end subroutine

!==============================================================================

subroutine test_join3()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoSpace
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize), allocatable :: strings_in(:)
  character(ssize) :: separator
  character(ssize) :: results
  character(ssize) :: expected
  integer(4) :: nst
  integer(4) :: istat
  
  ! === Preparation ===
  separator = " in-"
  strings_in  = [character(ssize) :: "1","2","3"]
  nst = size(strings_in)
  expected = "1 in-2 in-3"
  
  call StringUtility_join( strings_in, nst, results, separator, istat)
  
  call isEqual(results,expected)
  
  deallocate( strings_in )
  return
end subroutine




