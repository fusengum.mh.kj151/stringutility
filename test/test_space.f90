program test_space
  implicit none

  call test_NoSpace()
  call test_NoTab()
  call test_NoBlankChar()
end program

!==============================================================================
!==============================================================================
!==============================================================================

subroutine test_NoSpace()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoSpace
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  character(ssize), allocatable :: cases(:)
  integer :: i
  
  ! === Preparation ===
  cases    = [character(ssize) :: ]
  expected = [character(ssize) :: ]
  results  = [character(ssize) :: ]
 
  ! === Define test cases and their answers ===
  cases    = [ cases,    "Hello, new worlds!" ]
  expected = [ expected, "Hello,newworlds!" ]

  cases    = [ cases,    "Hello, new worlds!   " ]
  expected = [ expected, "Hello,newworlds!" ]
  
  cases    = [ cases,    "Hello,   new worlds!" ]
  expected = [ expected, "Hello,newworlds!" ]
  
  cases    = [ cases,    " Hello, new worlds!" ]
  expected = [ expected, "Hello,newworlds!" ]
  
  cases    = [ cases,    achar(z"09") // "Hello, new worlds!" ]
  expected = [ expected, achar(z"09") // "Hello,newworlds!" ]

  ! === Get results ===
  do i=1, size(cases)
    sdum = cases(i)
    call NoSpace(sdum)
    results = [ results, sdum ]
  end do 

  ! === Assertion ===
  do i=1,size(cases)
    call isEqual(results(i),expected(i))
  end do
  
  write(*,*) "Tests for 'NoSpace'  ---> OK"
  return
end subroutine

!==============================================================================

subroutine test_NoTab()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoTab
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  character(ssize), allocatable :: cases(:)
  integer :: i
  
  ! === Preparation ===
  cases    = [character(ssize) :: ]
  expected = [character(ssize) :: ]
  results  = [character(ssize) :: ]
 
  ! === Define test cases and their answers ===
  cases    = [ cases,    achar(z"09") // "Hello, new worlds!" ]
  expected = [ expected, "Hello, new worlds!" ]

  ! === Get results ===
  do i=1, size(cases)
    sdum = cases(i)
    call NoTab(sdum)
    results = [ results, sdum ]
  end do 

  ! === Assertion ===
  do i=1,size(cases)
    call isEqual(results(i),expected(i))
  end do
  
  write(*,*) "Tests for 'NoTab'  ---> OK"
  return
end subroutine

!==============================================================================

subroutine test_NoBlankChar()
  use TestSuite,     only: isEqual
  use StringUtility, only: NoBlankChar
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  character(ssize), allocatable :: cases(:)
  integer :: i
  
  ! === Preparation ===
  cases    = [character(ssize) :: ]
  expected = [character(ssize) :: ]
  results  = [character(ssize) :: ]
 
  ! === Define test cases and their answers ===
  cases    = [ cases,    achar(z"09") // "Hello, new worlds!" ]
  expected = [ expected, "Hello,newworlds!" ]

  ! === Get results ===
  do i=1, size(cases)
    sdum = cases(i)
    call NoBlankChar(sdum)
    results = [ results, sdum ]
  end do 

  ! === Assertion ===
  do i=1,size(cases)
    call isEqual(results(i),expected(i))
  end do
  
  write(*,*) "Tests for 'NoBlankChar'  ---> OK"
  return
end subroutine

!==============================================================================
