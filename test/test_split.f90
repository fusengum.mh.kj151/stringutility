program test_split_main
  implicit none

  call test_split_by_1letter()
  call test_split_by_string()
  call test_split_by_strings()
end program

!==============================================================================
!==============================================================================
!==============================================================================

subroutine test_split_by_1letter()
  use TestSuite,     only: isEqual
  use StringUtility, only: split
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  integer :: i,ndiv,istat
  
 
  !--- Test for space ---
  sdum = "Hello, new worlds!"
  expected = [ character(ssize) :: "Hello,","new", "worlds!" ]
  
  results  = [ character(ssize) :: "","","","","" ]
  call split(sdum,results,size(results)," ",ndiv,istat)

  do i=1,ndiv
    call isEqual(results(i),expected(i))
  end do
  
  deallocate( results )
  deallocate( expected )
  write(*,*) "Tests for 'split by 1 letter (1)'  ---> OK"

  !--- Test for arbitrary character ---
  sdum = "Hello,AnewAworlds!"
  expected = [ character(ssize) :: "Hello,","new", "worlds!" ]
  
  results  = [ character(ssize) :: "","","","","" ]
  call split(sdum,results,size(results),"A",ndiv,istat)

  do i=1,ndiv
    call isEqual(results(i),expected(i))
  end do
  
  deallocate( results )
  deallocate( expected )

  write(*,*) "Tests for 'split by 1 letter (2)'  ---> OK"
  return
end subroutine

!==============================================================================

subroutine test_split_by_string()
  use TestSuite,     only: isEqual
  use StringUtility, only: split
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  integer :: i,ndiv,istat
  
 
  sdum = "Hello,!ASnew!ASworlds!"
  expected = [ character(ssize) :: "Hello,","new", "worlds!" ]
  
  results  = [ character(ssize) :: "","","","","" ]
  call split(sdum,results,size(results),"!AS",ndiv,istat)

  do i=1,ndiv
    call isEqual(results(i),expected(i))
  end do
  
  deallocate( results )
  deallocate( expected )

  write(*,*) "Tests for 'Split by string'  ---> OK"
  return
end subroutine

!==============================================================================

subroutine test_split_by_strings()
  use TestSuite,     only: isEqual
  use StringUtility, only: split
  implicit none
  !---------------------------
  integer(4), parameter :: ssize = 512
  character(ssize) :: sdum
  character(ssize), allocatable :: delims(:)
  character(ssize), allocatable :: results(:)
  character(ssize), allocatable :: expected(:)
  integer :: i,ndiv,istat
  
 
  sdum = "Hello,!ASnew!BSworlds! EOF"
  expected = [ character(ssize) :: "Hello,","new", "worlds!","EOF" ]
  delims = [ character(ssize) :: "!AS","!BS"," ","AIBM" ]

  results  = [ character(ssize) :: "","","","","" ]
  call split(sdum,results,size(results),delims,ndiv,istat)

  do i=1,ndiv
    call isEqual(results(i),expected(i))
  end do
  
  deallocate( results )
  deallocate( expected )

  write(*,*) "Tests for 'Split by strings'  ---> OK"
  return
end subroutine

!==============================================================================











